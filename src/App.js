require("es6-promise").polyfill();

import React, { useState } from "react";
import fetchJsonp from "fetch-jsonp";
import logo from "./logo.svg";
import "./App.less";
import MPInfo from "./MPInfo";

export default function App() {
  // hold MP info
  const [mp, setMp] = useState(null);
  // input value
  const [postalCode, setPostalCode] = useState("");
  // show errors if response throws error
  const [responseError, setResponseError] = useState(false);
  // loading state
  const [loading, setLoading] = useState(false);

  // reponse when "Find MP" is clicked
  const handleSearch = async () => {
    const parseResponse = res => {
      const [mpInfo] = res.representatives_centroid.filter(
        rep => rep.elected_office === "MP"
      );

      console.log(mpInfo);
      setMp(mpInfo);
    };

    setMp(null);
    setLoading(true);
    setResponseError(false);

    try {
      const json = await fetchJsonp(
        "https://represent.opennorth.ca/postcodes/" +
          // convert to uppercase, remove whitespace
          postalCode.toUpperCase().replace(/\s/g, "") +
          "/"
      );
      const res = await json.json();

      parseResponse(res);
    } catch (err) {
      setResponseError(true);
    }
    setLoading(false);
  };

  const renderMpInfo = () => {
    if (loading) {
      return <div>Loading...</div>;
    } else if (mp !== null) {
      return <MPInfo mp={mp}>Mp INFO</MPInfo>;
    } else {
      return null;
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Canada MP Lookup</h1>
      </header>
      <div className="App-body">
        <p className="App-intro">Enter your postal code to find your MP!</p>
        <input
          className="App-input"
          value={postalCode}
          onChange={e => setPostalCode(e.target.value)}
        />
        <div>
          <button className="App-button" onClick={handleSearch}>
            Lookup MP
          </button>
        </div>
        {responseError && (
          <div style={{ color: "red" }}>
            Invalid postal code. Please try again.
          </div>
        )}
        {renderMpInfo()}
      </div>
    </div>
  );
}
