import React from "react";
import "./MPInfo.less";

export default function MPInfo({ mp }) {
  return (
    <div className="mp-info">
      <h1>Your MP is:</h1>
      <div className="row">
        <img className="image" alt={mp.first_name + " " + mp.last_name} src={mp.photo_url}></img>
        <div className="mp-info-details">
          <div className="mp-info-name">
            {mp.first_name + " " + mp.last_name}
          </div>
          <div>MP of {mp.district_name}</div>
          <small>{mp.party_name}</small>
          <div className="row">
            <a target="_blank" href={mp.url}>
              <button className="mp-info-link">View more info</button>
            </a>
            <a href={"mailto:" + mp.email.toLowerCase()}>
              <button className="mp-info-email">Send email</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
